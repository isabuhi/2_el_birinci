import React, { useEffect, useState} from 'react'
import {StyledBrands, StyledH3, StyledP} from "../StyledAppointment";
import { translate, getLanguage } from 'react-switch-lang';
import {Col, Container, Row} from "react-bootstrap";
import FIAT from "Assets/carsBrand/2elbirinci brand logo-Fiat.png";
import JEEP from "Assets/carsBrand/2elbirinci brand logo-Jeep.png";
import BMW from "Assets/carsBrand/2elbirinci brand logo-Bmw.png";
import HONDA from "Assets/carsBrand/2elbirinci brand logo-Honda.png";
import PSA from "Assets/carsBrand/2elbirinci brand logo-Psa.png";
import ALFA from "Assets/carsBrand/2elbirinci brand logo-Alfa Romeo.png";
import CITROEN from "Assets/carsBrand/2elbirinci brand logo-Citroen.png";
import MINI from "Assets/carsBrand/2elbirinci brand logo-Mini.png";
import OPEL from "Assets/carsBrand/2elbirinci brand logo-Opel.png";

function Brands ({t, setBrand, brand}){
    const [background, setBackground] = useState(false)
    const changeBackground = {background: "#1B4380"}
    const pngs = [
        {
            png: PSA,
            brand: "PSA"
        },
        {
            png: MINI,
            brand: "MINI"
        },
        {
            png: OPEL,
            brand: "OPEL"
        },
        {
            png: JEEP,
            brand: "JEEP"
        },
        {
            png: FIAT,
            brand: "FIAT"
        },
        {
            png: CITROEN,
            brand: "CITROEN"
        },
        {
            png: BMW,
            brand: "BMW"
        },
        {
            png: ALFA,
            brand: "ALFA"
        },
        {
            png: HONDA,
            brand: "HONDA"
        },

    ]



   return(
       <StyledBrands>
           <StyledH3>{t("appointment.tab2.h3")}</StyledH3>
           <StyledP>{t("appointment.tab2.p")}</StyledP>
           <Container className="container-900">
               <Row>
                   <Col sm={12}>
                       <Row className="my-row">
                           {
                               pngs.map((png, idx) => {
                                   return (
                                       <Col
                                           style={brand === png.brand? changeBackground: null}
                                            className='col_height'
                                            key={idx}
                                            sm={3}
                                            onClick={(e)=>{setBrand(png.brand); setBackground(true);}}
                                       >
                                           <img height="100%" src={png.png}/>
                                       </Col>
                                   )
                               })
                           }
                       </Row>
                   </Col>
               </Row>
           </Container>
       </StyledBrands>

   );
}

export default translate(Brands);