import React, { useEffect, useState} from 'react'
import {StyledPackets, StyledH3, StyledP} from "../StyledAppointment";
import { translate, getLanguage } from 'react-switch-lang';
import {Col, Container, Row} from "react-bootstrap";
import Car from "Assets/cars/mintosko-V4b2j7f1dfc-unsplash.jpg"



function Packets({t, packetS, setPacketName, packetName}){

    return(
        <StyledPackets>
            <StyledH3>{t(`appointment.carC.tab3.h3`)}</StyledH3>
            <StyledP>{t(`appointment.carC.tab3.p`)}</StyledP>
            <Row>
                {
                    eval(t(`appointment.carC.tab3.packets`)).map((packet, index)=>{

                        return (

                            <Col sm={4} onClick={(e)=>{packetS(true); setPacketName(packet.name);}} key={index}>
                                <div className="packet-box" style={packetName === packet.name? {background: "#005FAB"}: null} >
                                    <div >
                                        <h3 className="my-h3">{packet.name}</h3>
                                        <p></p>
                                        <div>
                                            {
                                                packet.desc.map((service, index)=>{
                                                    return <h5 className="my-h5" key = {index}>{service}</h5>
                                                })
                                            }
                                        </div>
                                    </div>
                                    <img className="packetImg" src={Car} />
                                </div>
                            </Col>
                        )

                    })
                }
            </Row>
        </StyledPackets>

    );

}

export default translate(Packets)