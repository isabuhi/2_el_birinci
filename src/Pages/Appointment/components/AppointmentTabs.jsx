import React, {useEffect, useState, useCallback} from 'react'
import { Tabs, Tab, Container, Row, Col } from 'react-bootstrap'
import {StyledHeadLine, StyledAppointmentTabs, StyledP, StyledH3} from '../StyledAppointment'
import Brands from './Brands'
import Forms from './Form'
import Packets from './Packets'
import ServiceList from './ServiceList'
import { translate } from 'react-switch-lang';


function AppointmentTabs({ t }) {

    const[brand, setBrand] = useState(null);
    const[tab, handleTabChange] = useState(null);
    const[serviceSelect, setServiceSelect] = useState(false);
    const[packetSelect, setPacketSelect] = useState(false);
    const[packet, setPacketName] = useState(false);


    useEffect(()=>
        {
            setBrand(null);
            setServiceSelect(false);
            setPacketSelect(false);
        }
    ,[tab])

    const setBrandTrigger = useCallback ( (brand)=>{

        setBrand(brand);

    },[]);







    return (
        <StyledAppointmentTabs>
            <Container>
                <StyledHeadLine>{t('appointment.tab1.h2')}</StyledHeadLine>
                <p className="text-start">{t('appointment.tab1.p')}</p>
                <Tabs

                    id="controlled-tab-example"
                    activeKey={tab}
                    onSelect={(k) => handleTabChange(k)}
                >
                    <Tab eventKey="quick"  title={t(`appointment.tabs.quick_service`)}>
                        <Brands
                            t = {t}
                            setBrand = {setBrandTrigger}
                            brand = {brand}
                            tab = {tab}
                        />

                        {

                            brand !== null?
                                <div>
                                    <StyledH3>{t(`appointment.quickS.tab3.h3`)}</StyledH3>
                                    <StyledP>{t(`appointment.quickS.tab3.p`)}</StyledP>
                                    <Forms t={t}/>
                                </div>
                            : null
                        }
                    </Tab>
                    <Tab eventKey="care" title={t(`appointment.tabs.car_care_service`)}>
                        <Brands
                            t = {t}
                            setBrand = {setBrandTrigger}
                            brand = {brand}
                        />
                        {

                            brand !== null ?
                                <>
                                    <Packets
                                        setPacketName={setPacketName}
                                        id="packet"
                                        t={t}
                                        packetS = {setPacketSelect}
                                        packetName={packet}
                                    />
                                    <ServiceList
                                        id = "service"
                                        t={t}
                                        serviceS = {setServiceSelect}
                                    />
                                </>

                                : null
                        }
                        {


                             serviceSelect || packetSelect ? <Forms/> : null
                        }

                    </Tab>

                </Tabs>
            </Container>

        </StyledAppointmentTabs>
    )
}

export default translate(AppointmentTabs);