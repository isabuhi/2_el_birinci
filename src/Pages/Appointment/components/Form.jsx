import React from 'react'
import { Form, Button, Row, Col, FloatingLabel} from 'react-bootstrap'
import {StyledForm} from '../StyledAppointment'
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import { translate, getLanguage } from 'react-switch-lang';

function Forms(){
    return(
        <StyledForm>


            <Form>
                <Row className="mb-4">
                    <Form.Label >Contact Information</Form.Label>
                    <Col sm={4} className="align-items-unset">

                        <Form.Group className="mb-3"  controlId="formBasicPassword">
                            <Form.Control type="text" placeholder="Surname" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">

                            <Form.Control type="text" placeholder="Lastname" />
                        </Form.Group>
                    </Col>

                    <Col sm={4} className="align-items-unset">
                        <Form.Group className="mb-3"  controlId="formBasicPassword">
                            <Form.Control type="email" placeholder="Email" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">

                            <Form.Control type="number" placeholder="Phone number" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Form.Label >Vehicle Information</Form.Label>
                    <Col sm={4} className="align-items-unset">
                        <Form.Group className="mb-3"  controlId="formBasicPassword">
                            <Form.Control type="text" placeholder="Brand Model"/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">

                            <Form.Control type="text" placeholder="Car Millage" />
                        </Form.Group>
                    </Col>

                    <Col sm={4} className="align-items-unset">
                        <Form.Group className="mb-3"  controlId="formBasicPassword">
                            <Form.Control type="email" placeholder="Car Year" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">

                            <Form.Control type="text" placeholder="Car Plate" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row className="mb-4" >
                    <Form.Label >Prefered Date</Form.Label>
                    <Col sm={4} className="align-items-unset">
                        <Datetime inputProps={{placeholder: "Desired Date"}} className="mb-4" timeFormat={false}/>
                        <Datetime inputProps={{placeholder: "Desired Time"}} className="mb-4" dateFormat={false}/>
                    </Col>
                </Row>
                <Row className="mb-4" >
                    <Col sm={2} className="align-items-unset">
                        <Form.Label >Additional Information</Form.Label>
                    </Col>
                    <Col sm={6} className="align-items-unset">
                        <FloatingLabel controlId="floatingTextarea2" label="Let us know what kind of service you want to make for your car . . .">
                            <Form.Control
                                rows="3"
                                as="textarea"
                                placeholder="Leave a comment here"
                                style={{ height: '100px' }}
                            />
                        </FloatingLabel>
                        <Button variant="white" type="submit">
                            Send
                        </Button>

                    </Col>
                </Row>

            </Form>
        </StyledForm>

    )
}

export default translate(Forms)