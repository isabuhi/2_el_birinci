import React, {useEffect, useState} from "react";
import { translate, getLanguage } from 'react-switch-lang';
import {Form, Button, Row, Col, Tab, Tabs} from 'react-bootstrap'
import {StyledServiceList, StyledH3, StyledP, StyledForm} from "../StyledAppointment";
import Forms from './Form'
import Select from 'react-select';




function ServiceList({t, serviceS}) {
    const [tab, handleTabChange] = useState(null);
    const styleP = {margin: "15px 0 6px", color: "#1B4380", fontWeight: "800"}


    useEffect(()=>{

        tab? serviceS(true): serviceS(false);
    },[tab])

    const serviceList =
        {
            1:
                {
                    btnText: "Brand New Car Services",
                    packets: [
                        {1: "Bionica Coating", 2: ["Bionica Coating"]},
                        {1: "Ceramic Coating", 2: ["Ceramic Coating"]},
                        {1: "Window Film", 2: ["Window Film"]},
                        {1: "Paint Protection Film", 2: ["Paint Protection Film"]},
                        {1: "Polyvinyl Coating", 2: ["Polyvinyl Coating"]},
                        {
                            1: "Car Soundproofing",
                            2: ["Hood Soundproofing", "Front Wheelarch And Fender", "Rear Wheelarch And Trunk Soundproofing", "Door Soundproofing", "Roof Soundproofing", "Floor Soundproofing"]
                        }
                    ]
                },
            2:
                {
                    btnText: "Used Car Services",
                    packets: [
                        {
                            1: "Car Care  Packets",
                            2: ["Ceramic Coating", "Ceramic Coating", "Window Film", "Paint Protection Film", "Polyvinyl Coating", "Car Sound Insulation"]
                        },
                        {
                            1: "Car Protection Packets",
                            2: ["Polish & Wax", "İnterior Cleaning", "Engine Cleaning", "Engine Protection", "Headlight Cleaning", "Air Conditioning And Bacteria Cleaning", "Glass Sealant"]
                        },
                        {
                            1: "Car Soundproofing",
                            2: ["Hood Soundproofing", "Front Wheelarch And Fender", "Rear Wheelarch And Trunk Soundproofing", "Door Soundproofing", "Roof Soundproofing", "Floor Soundproofing"]
                        }
                    ]
                }
        }


    return (
        <StyledServiceList>
            <StyledH3>Car Care Service List</StyledH3>
            <StyledP>If you are looking into more options for your Car Care, you can choose some of the service which
                can be found in below list.</StyledP>
            <Tabs

                id="controlled-tab-examples"
                activeKey={tab}
                onSelect={(k) => handleTabChange(k)}
            >
                <Tab eventKey="brandNewCar" className="service-tab" title={serviceList["1"].btnText}>
                    {
                        serviceList["1"].packets.map((packet, idx) => {

                            return (
                                <div id={idx}>
                                    <StyledP style={styleP}>{packet[1]}</StyledP>
                                    <Select
                                        className="my-select"
                                        isMulti
                                        options={packet["2"].map((service) => {
                                            return {value: service, label: service}
                                        })}
                                    />
                                </div>
                            )
                        })
                    }
                </Tab>
                <Tab eventKey="2ndHandCar" className="service-tab" title={serviceList["2"].btnText}>
                    {
                        serviceList["2"].packets.map((packet, idx) => {

                            return (
                                <div id={idx}>
                                    <StyledP style={styleP}>{packet[1]}</StyledP>
                                    <Select
                                        className="my-select"
                                        isMulti
                                        options={packet["2"].map((service) => {
                                            return {value: service, label: service}
                                        })}
                                    />
                                </div>
                            )
                        })
                    }
                </Tab>
            </Tabs>



        </StyledServiceList>

    )
}


export default translate(ServiceList)