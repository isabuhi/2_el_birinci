import styled from 'styled-components';

export const StyledAppointment = styled.div`
    width: 100vw;
    .banner{
        width: 100%;
        height: 65vh;
        background-color: var(--blue1);
        box-shadow: inset 0 -20px 20px 0px rgba(0,0,0,0.25);
        position: relative;
        &::before{
            position: absolute;
            content: '';
            width: 0;
            height: 0;
            left: 0;
            bottom: 0;
            border-bottom: 200px solid var(--white);
            border-left: 100vw solid transparent;
            z-index: 11;
            ${({ theme }) => theme.phone`
                border-bottom: 100px solid var(--white);
            `};
        }
        &::after{
            content: ' ';
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            opacity: 0.25;
            background-image: url('https://images.unsplash.com/photo-1485291571150-772bcfc10da5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1228&q=80');
            background-repeat: no-repeat;
            background-position: bottom;
            background-size: cover;
            background-attachment: fixed;
        }
        
        .car-img{
            bottom: -35vh;
            position: absolute;
            right: 2vw;
            z-index: 12;
            width: 50%;
            ${({ theme }) => theme.phone`
                width: 100%;
                bottom: -25vh;
            `};
        }
        .logo{
            width: 20vw;
            position: absolute;
            top: 5rem;
            left: 5rem;
            z-index: 12;
            img{
                width: 50%;
            }
            ${({ theme }) => theme.phone`
                left: -4rem;
                top: -2rem;
            `};
        }
        .main-head{
            position: absolute;
            bottom: 50%;
            width: 100%;
            z-index: 10;
            transform: skewY(-8deg) translateY(50%);
            h1{
                font-size: 11em;
                text-align: center;
                margin-bottom: 0;
                text-transform: uppercase;
                font-weight: bold;
                color: transparent;
                -webkit-text-stroke-width: 1px;
                -webkit-text-stroke-color: var(--white);
                ${({ theme }) => theme.phone`
                    font-size: 2.3em;
                `};
            }
            ${({ theme }) => theme.phone`
                transform: skewY(-15deg) translateY(50%);
            `};
        }   
    }
`

export const StyledAppointmentTabs = styled.section`
    margin: 5rem 0;
    .nav-tabs{
        position: relative;
        z-index: 14;
        margin-bottom: 5rem;
        border: none;
        display: flex;
        align-items: center;
        justify-content: center;
        li{
            button{
                background: var(--grey1);
                color: var(--blue1);
                padding: 20px 30px;
                border-radius: 0;
                &.active{
                    background: var(--blue1);
                    color: var(--white);
                }
                ${({ theme }) => theme.phone`
                    padding: 1rem;
                `};
            }
            &:first-child{
                button{
                    border-top-left-radius: 30px;
                    margin-right: 60px;
                }
            }
            &:last-child{
                button{
                    border-top-left-radius: 30px;
                }
            }
        }
    }
    .my-row{
        div{
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            img{
                margin: 0 auto;
            }
        }
    }
    p{
        text-align: center;
        margin-bottom: 2rem
    }
`
export const StyledServiceList = styled.section`
    
    .nav-tabs{
        position: relative;
        z-index: 14;
        margin-bottom: 1rem!important;
        border: none;
        display: flex;
        align-items: center;
        justify-content: space-around;
        width: 50%;
        li{
            flex: 0 0 auto;
            
            ${({ theme }) => theme.phone`
                width: unset!important;
            `};
            button{
                background: var(--grey1);
                color: var(--blue1);
                padding: 20px 30px;
                border-radius: 0;
                margin: unset!important;
                &.active{
                    background: var(--blue1);
                    color: var(--white);
                }
                ${({ theme }) => theme.phone`
                    padding: 1rem;
                    
                `};
            }
            &:first-child{
                button{
                    border-top-left-radius: 30px;
                }
            }
            &:last-child{
                button{
                    border-top-left-radius: 30px;
                }
            }
        }
    }
    p{
        text-align: center;
        margin-bottom: 2rem
    }
    .tab-content {
       display: flex;
       flex-direction: start
    }
    .service-tab {
       flex: 0 0 auto;
       width: 50%;
       ${({ theme }) => theme.phone`
                width: 100%!important;
            `};
    }
    .service-tab div{
      
       ${({ theme }) => theme.phone`
                width: 100%!important;
             
            `};
    }
    .my-p {
       p{
         margin: "15px 0 6px"
         color: blue;
       }
    }
    .my-select {
       &>div {
          border-radius: 0;
          border-color: #1B4380;
          box-shadow: 0px 3px 6px #1B438033;
       }
    }
`
export const StyledHeadLine = styled.h2`
    text-align: start;
    font-weight: bold;
    color: var(--blue1);
    position: relative;
    z-index: 12;
    font-size: 3rem;
    margin-bottom: 0;
    ${({ theme }) => theme.phone`
        font-size: 1.75rem;
    `};
   
`

export const StyledBrands = styled.div`
position: relative;
.container-900{
    max-width: 900px;
    width: 100%
       .row{
        height: 100%;
        flex-direction: row;
        flex-wrap: wrap;
        

            }
            .col_height{
            
            transition-duration: 200ms;
            height: 163px;
            margin: 25px;
            box-shadow: 0px 3px 6px #1B438033;
            border-radius: 20px 0px 0px 0px;
            &:hover {
              cursor: pointer;
              transform: scale(1.1);
            }
            ${({ theme }) => theme.phone`
                margin: 25px 0;
                transform: unset!important;
            `};
            
        }   
        .col-sm-12 {
            .row{
            flex-direction: row!important;
             }
         }       
`
export const StyledH3 = styled.h3`
        color: #1B4380;
        font-size: 24px;
        line-height : 29px;
        text-align: left;
        margin-bottom : 6px;
        font-weight: 900;
        margin-top: 60px;
    `

export const StyledP = styled.p`
        color: black;
        font-size: 16px;
        line-height : 21px;
        text-align: left!important;
    `

export const StyledForm = styled.section`
        padding: 100px 0;
        .align-items-unset {
            align-items: stretch!important;
            justify-content: flex-start!important;
            flex-direction: column;
            display: flex;
            
        }
        .form-control {
           box-shadow: 0px 3px 6px #1B438033;
           border-radius: 20px 0px 0px 0px;
           border: 1px solid #1B4380;
        }
        .form-label {
           color: #1B4380;
           font-size: 24;
           line-height: 29px;
           font-weight: 900;
        }
        button {
           width: min-content;
           align-self: end;
           margin-top: 66px;
           padding: 15px 60px;
           border-radius: 20px 0 0 0 ;
           box-shadow: 0px 3px 6px #1B438033;
           font-size: 30px;
           color: #1B4380;
           line-height: 36px;
           font-weight: 900;
           &:hover{
               background: #1B4380;
               color : white
           }
        }
        
    `

export const StyledPackets = styled.section`
        .row {
        .col-sm-4{
           padding: 0 30px;
            
        }
        }
        .packet-box {
            padding-bottom: 100%;
            position: relative;
            overflow: hidden;
            border-radius: 53px;
            box-shadow: 0px 3px 6px #00000029;
            background: #212529;
            transition-duration: 200ms;
            
            &:hover {
                transform: scale(1.09);
                cursor: pointer;
            }
            
            &>div {
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                position : absolute;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                padding: 60px 40px;
                z-index: 1;
               
            }
            
            
        }
        .packetImg {
            position: absolute;
            width: 100%;
            opacity: 0.5;
        }
        .my-h3 {
                    text-align: center;
                    color: white;
                    font-weight: 900;
                }
        .my-h5 {
            color: white;
            font-weight: 900;
            margin-bottom: 0;
        }
`
