import React, { useEffect, useState } from 'react'
import { StyledAppointment } from './StyledAppointment'
import Car from 'Assets/cars/appointment.png';
import { useDispatch } from 'react-redux';
import { translate, getLanguage } from 'react-switch-lang';
import AppointmentTabs from "./components/AppointmentTabs";

function Appointment(props) {

    const dispatch = useDispatch();
    const { t } = props;
    const activeLang = getLanguage();



    return (
        <StyledAppointment>
            <div className="banner">
                <img className="car-img" src={Car} alt="" />
                <div className="main-head">
                    <h1>{t(`sidebar.li6`)}</h1>
                </div>
            </div>
            <AppointmentTabs
                t={t}
            />
        </StyledAppointment>
    )
}

export default translate(Appointment);