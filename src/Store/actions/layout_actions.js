import { OPEN_SIDEBAR } from 'Store/types';

export function SideBarOpenAction(){
    return{
        type: OPEN_SIDEBAR,
        payload: null
    }
}