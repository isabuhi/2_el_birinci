import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
     .main-head{
            position: absolute;
            bottom: 50%;
            width: 100%;
            z-index: 10;
            transform: skewY(-6deg) translateY(50%);
            h1{
                font-size: 11em;
                text-align: center;
                margin-bottom: 0;
                text-transform: uppercase;
                font-weight: bold;
                color: transparent;
                -webkit-text-stroke-width: 1px!important;
                -webkit-text-stroke-color: var(--white);
                ${({ theme }) => theme.phone`
                    font-size: 2.3em;
                `};
            }
            ${({ theme }) => theme.phone`
                transform: skewY(-15deg) translateY(50%);
            `};
    }   
    html{
        line-height: normal;
        --blue1: #1b4380;
        --blue2: #426495;
        --blue3: #adbbce;
        --red: #e2000f;
        --grey1: #f5f5f5;
        --grey2: #ececec;
        --grey3: #cccccc;
        --grey4: #777777;
        --white: #ffffff;
        --black: #515151;
        body{
            overflow-x: hidden !important;
        }
        button:focus{
            outline: none !important;
            box-shadow: none !important;
            border: none !important;
        }
        .form-control:focus{
            outline: none !important;
            box-shadow: none !important;
        }
        section{
            padding: 5rem 0;
            background: var(--white);
        }  
    }
    ${({ theme }) => theme.phone`
        html{
            button{
                border-width: 1px !important;
            }
        }
    `}
`

export default GlobalStyle;